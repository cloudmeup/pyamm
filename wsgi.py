# -*- coding: utf-8 -*-
"""Wsgi script.

Usage:
FLASK_APP=./wsgi.py flask run"""

import sys
import os

project = "pyamm"  #pylint: disable=invalid-name

VENV = 'venv'

# Use instance folder, instead of env variables.
# specify dev/production config
#os.environ['%s_APP_CONFIG' % project.upper()] = ''
# http://code.google.com/p/modwsgi/wiki/ApplicationIssues#User_HOME_Environment_Variable
#os.environ['HOME'] = pwd.getpwuid(os.getuid()).pw_dir

# activate virtualenv
def virtualenv():
    """Activate the virtualenv"""
    dir_path = os.path.dirname(os.path.realpath(__file__))
    activate_this = os.path.join(dir_path, VENV, "bin/activate_this.py")
    execfile(activate_this, dict(__file__=activate_this))

BASE_DIR = os.path.join(os.path.dirname(__file__))
if BASE_DIR not in sys.path:
    sys.path.append(BASE_DIR)

virtualenv()

#import logging
# https://docs.python.org/2/howto/logging.html
# https://gist.github.com/ibeex/3257877
#logging.basicConfig(level=logging.DEBUG)
#logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#logging.info('Started')

# give wsgi the "application"
from pyamm import create_app #pylint: disable=wrong-import-position
application = create_app()   #pylint: disable=invalid-name

#@application.cli.command()
#def setdir():
#    """Create app system directory"""
#    from pyamm.utils import INSTANCE_FOLDER_PATH
#    if os.path.isdir(INSTANCE_FOLDER_PATH):
#        pass
#    else:
#        os.makedirs(INSTANCE_FOLDER_PATH)
#
#@application.cli.command()
#def cov():
#    """Runs the unit tests with coverage."""
#    print 'Please run the script ./get_coverage.py'
#
#@application.cli.command()
#def test():
#    """Runs the unit tests."""
#    print 'please run python -m unittest discover'
