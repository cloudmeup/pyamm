#--------------------------------------------------------------------------
# LDAP Schema for phamm and getmail
#----------------------------------
# Release 0.1
# 2016/06/07
#--------------------------------------------------------------------------
# Copyright (c) 2016 Riccardo Scartozzi - http://softwareworkers.it
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 2
# or any later version published by the Free Software Foundation;
#--------------------------------------------------------------------------
# 1.3.6.1.4.1.32589         Software Workers srl's OID
# 1.3.6.1.4.1.32589.1       Phamm
# 1.3.6.1.4.1.32589.1.2     getmail
# 1.3.6.1.4.1.32589.1.2.1   AttributeTypes
# 1.3.6.1.4.1.32589.1.2.2   ObjectClasses
#--------------------------------------------------------------------------

# Attribute Types
#-----------------

attributeType ( 1.3.6.1.4.1.32589.1.2.1.10
        NAME 'connOutboundEnable'
        DESC 'Set if the account is enabled for sending emails through connector'
        EQUALITY booleanMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.11
        NAME 'connSMTPServer'
        DESC 'The URI of the remote SMTP server, in format smtps://uri.com'
        EQUALITY caseIgnoreIA5Match
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.13
        NAME 'connInboundEnable'
        DESC 'Enable the download via getmail'
        EQUALITY booleanMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.14
        NAME 'connProtocol'
        DESC 'Getmail protocol type'
        EQUALITY caseExactIA5Match
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.15
        NAME 'connRemoteServer'
        DESC 'URI of the remote IMAP/POP server'
        EQUALITY caseExactIA5Match
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.16
        NAME 'connLogin'
        DESC 'User login'
        EQUALITY caseIgnoreIA5Match
        SUBSTR caseIgnoreIA5SubstringsMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.17
        NAME 'connPassword'
        DESC 'user password'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.18
        NAME 'connDelete'
        DESC 'If set True delete the message from the remote server'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.19
        NAME 'connFolders'
        DESC 'Folders to download'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

# Second group
attributeType ( 1.3.6.1.4.1.32589.1.2.1.20
        NAME 'connOutboundEnable1'
        DESC 'Set if the account is enabled for sending emails through connector'
        EQUALITY booleanMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.21
        NAME 'connSMTPServer1'
        DESC 'The URI of the remote SMTP server, in format smtps://uri.com'
        EQUALITY caseIgnoreIA5Match
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.23
        NAME 'connInboundEnable1'
        DESC 'Enable the download via getmail'
        EQUALITY booleanMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.24
        NAME 'connProtocol1'
        DESC 'Getmail protocol type'
        EQUALITY caseExactIA5Match
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.25
        NAME 'connRemoteServer1'
        DESC 'URI of the remote IMAP/POP server'
        EQUALITY caseExactIA5Match
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.26
        NAME 'connLogin1'
        DESC 'User login'
        EQUALITY caseIgnoreIA5Match
        SUBSTR caseIgnoreIA5SubstringsMatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.27
        NAME 'connPassword1'
        DESC 'user password'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.28
        NAME 'connDelete1'
        DESC 'If set True delete the message from the remote server'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )

attributeType ( 1.3.6.1.4.1.32589.1.2.1.29
        NAME 'connFolders1'
        DESC 'Folders to download'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )

# Classes
#---------

objectClass ( 1.3.6.1.4.1.32589.1.2.2.1
        NAME 'connExternalMailAccount'
        DESC 'IMAP-SMTP external mail account, via postfix/getmail'
        SUP top AUXILIARY
        MUST ( connOutboundEnable $ connInboundEnable )
        MAY ( connSMTPServer $ connProtocol $ connRemoteServer $ connLogin $ connPassword $ connDelete $ connFolders ) )

objectClass ( 1.3.6.1.4.1.32589.1.2.2.2
        NAME 'connExternalMailAccount1'
        DESC 'IMAP-SMTP external mail account, via postfix/getmail'
        SUP top AUXILIARY
        MUST ( connOutboundEnable1 $ connInboundEnable1 )
        MAY ( connSMTPServer1 $ connProtocol1 $ connRemoteServer1 $ connLogin1 $ connPassword1 $ connDelete1 $ connFolders1 ) )

