# PyAmm

This project is inspired by [phamm](https://github.com/lota/phamm).

It allows to set up dashboards to easily manage services that use an LDAP
backend for accounts and configuration.

PyAmm is written in Python, and aims to provide a fast and easy to use
interface (both for admins and end users).

## How to install

### Env

Using `virtualenv` is recommended:

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Required packages

If using a Debian-based system, the following packages should be installed:

```
sudo apt install python-dev virtualenv libmysqlclient-dev build-essential libffi-dev
```

### LDAP

On Debian-based systems, the following packages should be installed on the
LDAP backend node(s):

```
sudo apt install slapd ldap-utils
```

For LDAP schemas and example configurations please refer to the `example`
directory.

## Translation

Translations are managed throught flask-babel.

There are two way for translating depending on context (a Jinja2 template file
or Python code).

### Jinja

```
{% trans %}foo{% endtrans %}

{% trans num %}
There is {{ num }} object.
{% pluralize %}
There are {{ num }} objects.
{% endtrans %}

{{ _('Hello World!') }}
```

### Python code

```
from flask_babel import gettext as _

print _(Hi, this will be translated)
```

### Creating new localizations

To update the source:

```
pybabel extract -F babel.cfg -o messages.pot ./pyamm
pybabel init -i messages.pot -d translations -l de
pybabel compile -d translations
```

If just updating an existing localizon, run:

```
pybabel update -i messages.pot -d translations
```

## Running PyAmm

A local test/development environment can be run using Flask's own development
server:

```
source venv/bin/activate
FLASK_APP=./wsgi.py FLASK_TEST=1 flask run --port 5000
```

## Configuration

The default configuration can be overridden via a custom configuration file
placed in `/etc/pyamm/config.py`

##  Security

Security should be enforced at two levels:

 * Flask user permissions
 * LDAP user permissions (every user has a different connection to LDAP)

## References

 * [flask-babel module](https://pythonhosted.org/Flask-Babel/)
 * [Jinja2 l18n](http://jinja.pocoo.org/docs/2.9/templates/#i18n-in-template)
 * [Localization examples](https://phraseapp.com/blog/posts/python-localization-for-flask-applications/)
