# -*- coding: utf-8 -*-
"""Interface for flask_login amministration. Here the user can log in/out"""
#pylint: disable=unused-import
from uuid import uuid4
from flask_babel import gettext as _
from flask import Blueprint, render_template, current_app, request, flash, \
    url_for, redirect, session, abort
from flask_login import login_required, login_user, current_user, logout_user, \
    confirm_login, login_fresh

from pyamm.user import User
from pyamm.extensions import ldap, login_manager
from pyamm.frontend.forms import LoginForm
from module.ldap3login import AuthenticationResponseStatus


frontend = Blueprint('frontend', __name__)  #pylint: disable=invalid-name


@frontend.route('/', methods=['GET', 'POST'])
def home():
    """Interface for homepage and user login."""
    if current_user.is_authenticated:
        flash(_("You're already logged in as {}").format(current_user.username),
              "success")
        return redirect(url_for('accounts.main'))

    form = LoginForm(login=request.args.get('login', None),
                     next=request.args.get('next', None))
    if form.validate_on_submit():
        result = User.authenticate(form.login.data, form.password.data)
        if result.status == AuthenticationResponseStatus.success: #pylint: disable=no-member
            user = ldap._save_user(result.user_dn,  #pylint: disable=protected-access
                                   result.user_id,
                                   result.user_info,
                                   result.user_groups)
            if login_user(user):
                return redirect(form.next.data or url_for('accounts.main'))
            else:
                flash(_('Sorry, invalid login'), 'danger')
        else:
            flash(_('Sorry, invalid login'), 'danger')
    return render_template('frontend/login.html', form=form)


#@frontend.route('/reauth', methods=['GET', 'POST'])
#@login_required
#def reauth():
#    form = ReauthForm(next=request.args.get('next'))
#
#    if request.method == 'POST':
#        user, authenticated = User.authenticate(current_user.name,
#                                    form.password.data)
#        if user and authenticated:
#            confirm_login()
#            flash('Reauthenticated.', 'success')
#            return redirect('/change_password')
#
#        flash('Password is wrong.', 'danger')
#    return render_template('frontend/reauth.html', form=form)


@frontend.route('/logout')
@login_required
def logout():
    """Interface for user logout."""
    logout_user()
    session.pop('ldapPW', None)
    flash(_('Logged out'), 'success')
    return redirect(url_for('frontend.home'))


#@frontend.route('/signup', methods=['GET', 'POST'])
#def signup():
#    if current_user.is_authenticated:
#        return redirect(url_for('user.profile'))
#    return redirect(url_for('index'))
#
#    form = SignupForm(next=request.args.get('next'))
#
#    if form.validate_on_submit():
#        user = User()
#        form.populate_obj(user)
#
#        db.session.add(user)
#        db.session.commit()
#
#        if login_user(user):
#            flash('Signed up', 'success')
#            return redirect(form.next.data or url_for('user.profile'))
#
#    return render_template('frontend/signup.html', form=form)


##@frontend.route('/change_password', methods=['GET', 'POST'])
#def change_password():
#    user = None
#    if current_user.is_authenticated:
#        if not login_fresh():
#            return login_manager.needs_refresh()
#        user = current_user
#    elif 'activation_key' in request.values and 'email' in request.values:
#        activation_key = request.values['activation_key']
#        email = request.values['email']
#        user = User.query.filter_by(activation_key=activation_key) \
#                         .filter_by(email=email).first()
#
#    if user is None:
#        abort(403)
#
#    form = ChangePasswordForm(activation_key=user.activation_key)
#
#    if form.validate_on_submit():
#        user.password = form.password.data
#        user.activation_key = None
#        db.session.add(user)
#        db.session.commit()
#
#        flash("Your password has been changed, please log in again", "success")
#        return redirect(url_for("frontend.login"))
#
#    return render_template("frontend/change_password.html", form=form)
#
#
#@frontend.route('/reset_password', methods=['GET', 'POST'])
#def reset_password():
#    form = RecoverPasswordForm()
#
#    if form.validate_on_submit():
#        user = User.query.filter_by(email=form.email.data).first()
#
#        if user:
#            flash('Please see your email for instructions on '
#                  'how to access your account', 'success')
#
#            user.activation_key = str(uuid4())
#            db.session.add(user)
#            db.session.commit()
#
#            return render_template('frontend/reset_password.html', form=form)
#        else:
#            flash('Sorry, no user found for that email address', 'error')
#
#    return render_template('frontend/reset_password.html', form=form)
