# -*- coding: utf-8 -*-
"""Define here the basic extensions (then configure them in app.py)"""

#pylint: disable=invalid-name,wrong-import-position
from flask_login import LoginManager
login_manager = LoginManager()

from module.ldap3login import LDAP3LoginManager
ldap = LDAP3LoginManager()

from flask_babel import Babel
babel = Babel()
