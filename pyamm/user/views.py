# -*- coding: utf-8 -*-
"""Views for basic account administration.

Missing: password recover
"""

#pylint: disable=unused-import
from flask import Blueprint, render_template, send_from_directory, request, \
    current_app, flash, session
from pyamm.user.models import User


user = Blueprint('user', __name__, url_prefix='/user') #pylint: disable=invalid-name

