# -*- coding: utf-8 -*-
"""Interface for administering emails and connectors.

SERVICE LEVEL:
    0- server: not required
    1- domain: check dns, enable domain
    2- user: accounts and aliases
    3- connector: pull and push parameters
"""
#pylint: disable=unused-import
import re
from flask_babel import gettext as _
from flask_wtf import Form
from flask_wtf.html5 import URLField, EmailField, TelField
from wtforms import ValidationError, StringField, PasswordField, SubmitField, \
    TextAreaField, FileField, DateField, DateTimeField, SelectField, BooleanField, \
    IntegerField
from wtforms.validators import DataRequired, Length, EqualTo, Email, URL, \
        AnyOf, Optional
from wtforms.widgets import PasswordInput
from wtforms_components import read_only
from flask import render_template, flash, url_for, redirect, current_app, request
from flask_login import login_required, current_user
from ldap3 import ALL, SUBTREE, Connection, Server, ObjectDef, AttrDef, Reader

from pyamm.fancy.utils import BaseForm
import pyamm.fancy.utils as utils


def account_form(nav):
    """Return the form"""
    form = None
    if nav.action == 'account':
        if nav.level == 2:
            form = BaseForm.create('form_account_account')
            form.append_ro_field('pyAccount', EmailField(_('User ID (Email)')))
            pw_field = BaseForm.setfield({'field': 'Shapassword',
                                          'label': _('Password'),
                                          'rawrequirements': \
                                   [EqualTo('confirm', message=_('Passwords must match')),
                                    Length(min=current_app.config['PASSWORD_MIN'],
                                           max=current_app.config['PASSWORD_MAX'])]})
            form.append_field('userPassword', pw_field)
            form.append_field('confirm', PasswordField(_('Confirm your new password')))
            lang_type = BaseForm.setfield({'field': 'Select',
                                           'label': _('Set user language'),
                                           'options': [{'it': _('Italian')},
                                                       {'en': _('English')}]})
            form.append_field('pyUserLanguage', lang_type)
            form.append_field('submit',
                              SubmitField('Modify account',
                                          render_kw={"class": "btn btn-success"}))
            form.noedit.append('pyAccount')
    return form


def account_form_sys(nav):
    """Return the form"""
    form = None
    if nav.action == 'account':
        if nav.level == 2:
            form = BaseForm.create('form_account_account_sys')
            form.append_ro_field('pyAccount', EmailField(_('User ID (Email)')))
            pw_field = BaseForm.setfield({'field': 'Shapassword',
                                          'label': _('Password'),
                                          'rawrequirements': \
                                   [Length(min=current_app.config['PASSWORD_MIN'],
                                           max=current_app.config['PASSWORD_MAX'])]})
            form.append_field('userPassword', pw_field)
            form.append_field('pyDomainAdmin', BooleanField(_('Is a domain administrator')))
            form.append_field('pySystemAdmin', BooleanField(_('Is a system administrator')))
            lang_type = BaseForm.setfield({'field': 'Select',
                                           'label': _('Set user language'),
                                           'options': [{'it': _('Italian')},
                                                       {'en': _('English')}]})
            form.append_field('pyUserLanguage', lang_type)
            form.append_field('submit',
                              SubmitField(_('Modify account'),
                                          render_kw={"class": "btn btn-success"}))
            form.noedit.append('pyAccount')
    return form


def account_form_domain(nav):
    """Return the form"""
    form = None
    if nav.action == 'account':
        if nav.level == 2:
            form = BaseForm.create('form_account_account_domain')
            form.append_ro_field('pyAccount', EmailField(_('User ID (Email)')))
            pw_field = BaseForm.setfield({'field': 'Shapassword',
                                          'label': _('Password'),
                                          'rawrequirements': \
                                   [Length(min=current_app.config['PASSWORD_MIN'],
                                           max=current_app.config['PASSWORD_MAX'])]})
            form.append_field('userPassword', pw_field)
            form.append_field('pyDomainAdmin', BooleanField(_('Is a domain administrator')))
            lang_type = BaseForm.setfield({'field': 'Select',
                                           'label': _('Set user language'),
                                           'options': [{'it': _('Italian')},
                                                       {'en': _('English')}]})
            form.append_field('pyUserLanguage', lang_type)
            form.append_field('submit',
                              SubmitField(_('Modify account'),
                                          render_kw={"class": "btn btn-success"}))
            form.noedit.append('pyAccount')
    return form


def account_add(nav):
    """Display object options"""
    if nav.action == 'account':
        if nav.level == 1:
            domain = nav.path[0]
            search_f = '(&(objectClass=pyAccount)(pyAccount={}))'
            form = BaseForm.create('form_AddUser_{}'.format(domain))
            form.append_field('obj', StringField(_('new account'),
                                                 [DataRequired(), Email(), CheckEmail(domain),
                                                  CheckUniq(search_f)]))
            form.append_field('submit',
                              SubmitField(_('Create new account'),
                                          render_kw={"class": "btn btn-success"}))
            return form()
    elif nav.action == 'domain':
        if nav.level == 0:
            search_f = '(&(objectClass=pyDomain)(pyDomain={}))'
            form = BaseForm.create('form_AddDomain')
            form.append_field('obj', StringField(_('new domain'),
                                                 [DataRequired(), CheckUniq(search_f)]))
            form.append_field('submit',
                              SubmitField(_('Create new domain'),
                                          render_kw={"class": "btn btn-success"}))
            return form()
    return None

class CheckEmail(object): #pylint: disable=too-few-public-methods
    """Check it the fild.data domain's part is the same as pointed"""
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def sanitize(field):
        """Check that there is:
        - only one @
        - no -
        - only alphaNUM
        """
        if field.data.count('@') != 1:
            raise ValidationError(_("The email address needs to have one and only one '@'"))
        if not re.match(r"^[A-Za-z0-9_\.@]*$", field.data):
            raise ValidationError(_("Only letters, digits and '.' or '_' are accepted"))

    def isdomainrelated(self, field):
        """Check if it's part of the domain"""
        if field.data.split('@')[-1] != self.domain:
            msg = _("The email has to be part of the domain @{}")
            raise ValidationError(msg.format(self.domain))

    def __call__(self, form, field):
        self.sanitize(field)
        self.isdomainrelated(field)

class CheckUniq(object):  #pylint: disable=too-few-public-methods
    """Verify that the object is uniq and does not already exists.
    Usage:
      CheckUniq('(Account={})')
    """
    top_dn = 'o=pyamm,dc=example,dc=tld'

    def __init__(self, search_filter):
        self.connection = utils.get_ldap()
        self.search_filter = search_filter

    def search(self, obj):
        """Perform an LDAP search against obj"""
        search_filter = self.search_filter.format(obj)
        self.connection.search(self.top_dn, search_filter)
        if len(self.connection.entries) == 0:
            return False
        else:
            return True

    def __call__(self, form, field):
        if self.search(field.data):
            raise ValidationError(_("{} already exists").format(field.data))
        else:
            pass
