# -*- coding: utf-8 -*-
"""Interface for administering emails and connectors.

SERVICE LEVEL:
    0- server: not required
    1- domain: check dns, enable domain
    2- user: accounts and aliases
    3- connector: pull and push parameters
"""
from flask import Blueprint, render_template, flash, url_for, redirect, current_app, request #pylint: disable=unused-import
from flask_login import login_required
import pyamm.fancy.utils as utils
import forms #pylint: disable=relative-import

email = Blueprint('email', __name__, url_prefix='/email')  #pylint: disable=invalid-name


class Email(utils.Pyamm):
    """Email decorator"""
    path = 'email.mail'
    module_layout = {
        'account': {
            'level_2': {
                'attributes': {
                    'MailType': {'field': 'String',
                                 'label': 'Email type'},
                    'VirtualMailActive': {'field': 'Boolean',
                                          'label': 'email is active'},
                    'VirtualMaildrop': {'field': 'String',
                                        'label': 'Alias list',
                                        'multiple': True},
                    'pyAccount': {'field': 'Hidden',
                                  'label': 'Account'},
                    'userPassword': {'field': 'Shapassword',
                                     'label': 'password'}},
                'objectclass': ['pyAccount', 'pyVirtualAccount'],
                'rnd': 'pyAccount'},
            'table_0': {'attributes': {'pyDomain': {'field': 'index',
                                                    'label': 'domain',
                                                    'order': 1}},
                        'filter': '(objectClass=pyDomain)'},
            'table_1': {'attributes': {'MailType': {'field': 'string',
                                                    'label': 'Type of account',
                                                    'order': 5},
                                       'VirtualMailActive': {'field': 'boolean',
                                                             'label': 'Active status',
                                                             'order': 10},
                                       'pyAccount': {'field': 'index',
                                                     'label': 'username',
                                                     'order': 1}},
                        'filter': '(objectClass=pyAccount)',
                        'objectclass': ['pyAccount', 'pyVirtualAccount']}},
        'connector': {
            'level_3': {
                'attributes': {
                    'pyConnFetchFolders': {'field': 'String',
                                           'label': 'Folder to download'},
                    'pyConnInboundEnabled': {'field': 'Boolean',
                                             'label': 'Enable fetching emails'},
                    'pyConnLocalOwner': {'label': 'Remote server URL'},
                    'pyConnOutboundEnabled': {'field': 'Boolean',
                                              'label': 'Enable sending emails'},
                    'pyConnPullProtocol': {'field': 'Select',
                                           'label': 'Type of protocol connection'},
                    'pyConnPullURL': {'field': 'URL',
                                      'label': 'Remote server URL'},
                    'pyConnRemoteDelete': {'label': 'Remote server URL'},
                    'pyConnSMTPAuth': {'label': 'Remote server URL'},
                    'pyConnSMTPURL': {'label': 'Remote server URL'},
                    'pyConnUserLogin': {'label': 'Remote server URL'},
                    'pyConnUserPassword': {'label': 'Remote server URL'},
                    'pyMailConnector': {'field': 'Hidden',
                                        'label': 'Account'}},
                'objectclass': ['pyMailConnector'],
                'rnd': 'pyMailConnector',
                'modifier': 'connector_ldap_creation'},
            'table_0': {'attributes': {'pyDomain': {'field': 'index',
                                                    'label': 'domain',
                                                    'order': 1}},
                        'filter': '(objectClass=pyDomain)'},
            'table_1': {'attributes': {'pyAccount': {'field': 'index',
                                                     'label': 'account',
                                                     'order': 1}},
                        'filter': '(objectClass=pyAccount)'},
            'table_2': {'attributes': {'pyMailConnector': {'field': 'index',
                                                           'label': 'connector',
                                                           'order': 1}},
                        'filter': '(objectClass=pyMailConnector)'}},
        'domain': {
            'level_1': {
                'attributes': {
                    'pyDelete': {'field': 'Boolean',
                                 'label': 'Mark as to be deleted'},
                    'pyDomain': {'field': 'Hidden',
                                 'label': 'Domain'},
                    'pyDomainMTAstatus': {'field': 'Boolean',
                                          'label': 'Enable the domain as MTA destination'}},
                'objectclass': ['pyDomain', 'pyVirtualDomain'],
                'rnd': 'pyDomain'},
            'table_0': {'attributes': {'pyDelete': {'field': 'boolean',
                                                    'label': 'Marked for deletion',
                                                    'order': 3},
                                       'pyDomain': {'field': 'index',
                                                    'label': 'domain',
                                                    'order': 1},
                                       'pyDomainMTAstatus': {'field': 'boolean',
                                                             'label': 'Enabled as MTA destination',
                                                             'order': 2}},
                        'filter': '(objectClass=pyDomain)'}}}


    def set_template(self, nav, user): #pylint: disable=no-self-use,unused-argument
        """Templateing"""
        if nav.action == 'connector':
            return 'panel/email.connector.html'
        else:
            return 'panel/email.html'

    def check_permission(self, nav, user):
        """If the user permissions are not valid or out of bounds then redirect
        to a valid resource.
        """
        # check user permission
        if user.level == 1 and nav.path[0] != user.domain:
            url = url_for('email.mail', action=nav.action, domain=user.domain)
            return redirect(url, 302)
        elif user.level == 2 and (nav.path[0] != user.domain or nav.path[1] != user.username):
            url = url_for(self.path, action=nav.action,
                          domain=user.domain, account=user.username)
            return redirect(url, 302)
        # check module limits
        if nav.action == 'domain':
            if nav.path[1]:
                return redirect(url_for(self.path, action=nav.action, domain=nav.path[0]), 302)
        elif nav.action == 'account':
            if nav.path[2]:
                return redirect(url_for(self.path, action=nav.action,
                                        domain=nav.path[0], account=nav.path[1]), 302)
        return True

    def set_options(self, nav, user):
        """Display object options"""
        if nav.action == 'account' and nav.level == 2:
            ldap = self.ldap_object(nav)
            if ldap:
                entry = ldap.get()
            else:
                entry = None
            form = forms.email_account_form(nav)
        elif nav.action == 'domain' and nav.level == 1:
            ldap = self.ldap_object(nav)
            if ldap:
                entry = ldap.get()
            else:
                entry = None
            form = forms.email_domain_form(nav)
        elif nav.action == 'connector' and nav.level == 3:
            ldap = self.ldap_object(nav)
            if ldap:
                entry = ldap.get()
            else:
                entry = None
            form = forms.email_connector_form(nav)
        else:
            return None
        if entry:
            return form(obj=entry)
        else:
            return form()

    def set_new(self, nav, user):
        """Form for new elementes"""
        return forms.email_add(nav)

    @staticmethod
    def connector_ldap_creation(_self):
        """Function used by the pre-formatting of custom connectors value.
        """
        for attribute in _self.list_attributes():
            _self.set_wentry_attribute(attribute)
        attribute = 'pyConnLocalOwner'
        new_value = _self.nav.path[1]
        if hasattr(_self.wentry, attribute):
            old_value = getattr(_self.wentry, attribute).value
        else:
            old_value = None
        if new_value != old_value:
            setattr(_self.wentry, attribute, new_value)
        attribute = 'pyConnSMTPAuth'
        if hasattr(_self.filtered_entry, 'pyConnUserLogin') and \
                hasattr(_self.filtered_entry, 'pyConnUserPassword'):
            new_value = ':'.join([getattr(_self.filtered_entry, 'pyConnUserLogin'),
                                  getattr(_self.filtered_entry, 'pyConnUserPassword')])
            if hasattr(_self.wentry, attribute):
                old_value = getattr(_self.wentry, attribute).value
            else:
                old_value = None
            if new_value != old_value:
                setattr(_self.wentry, attribute, new_value)

view = login_required(Email.as_view('mail')) #pylint: disable=invalid-name
email.add_url_rule('/', view_func=view)
email.add_url_rule('/<action>/', view_func=view)
email.add_url_rule('/<action>/<domain>/', view_func=view)
email.add_url_rule('/<action>/<domain>/<account>/', view_func=view)
email.add_url_rule('/<action>/<domain>/<account>/<resource>/', view_func=view)

