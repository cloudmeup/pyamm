# -*- coding: utf-8 -*-
"""Application configuration goes here."""
#pylint: disable=unused-import,missing-docstring,too-few-public-methods
import os

from pyamm.utils import make_dir, INSTANCE_FOLDER_PATH


class BaseConfig(object):

    PROJECT = "pyamm"

    # Get app root path, also can use flask.root_path.
    # ../../config.py
    PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

    DEBUG = False
    TESTING = False

    ADMINS = ['youremail@yourdomain.com']

    # http://flask.pocoo.org/docs/quickstart/#sessions
    SECRET_KEY = 'my super secret keykvekvdakmev'

    LOG_FOLDER = os.path.join(INSTANCE_FOLDER_PATH, 'logs')

    LANGUAGES = {'en': 'English',
                 'it': 'Italiano'}
    BABEL_DEFAULT_LOCALE = 'it'

    # Fild upload, should override in production.
    # Limited the maximum allowed payload to 16 megabytes.
    # http://flask.pocoo.org/docs/patterns/fileuploads/#improving-uploads
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    UPLOAD_FOLDER = os.path.join(INSTANCE_FOLDER_PATH, 'uploads')

    #LDAP
    LDAP_HOST = '127.0.0.1'
    LDAP_USE_SSL = False
    LDAP_READONLY = False
    #LDAP_BASE = 'dc=example,dc=tld'
    LDAP_BIND_USER_DN = None
    LDAP_BIND_USER_PASSWORD = None
    LDAP_GET_USER_ATTRIBUTES = ['pySystemAdmin', 'pyDomainAdmin', 'pyUserLanguage']
    LDAP_USER_OBJECT_FILTER = '(objectclass=pyAccount)'
    # Do a direct bind to the server for testing the user password
    #
    # LDAP login options
    LDAP_BIND_DIRECT_CREDENTIALS = True
    LDAP_DOMAIN_ATTR = 'pyDomain'
    LDAP_BASE_DN = 'o=pyamm,dc=example,dc=tld'
    LDAP_USER_LOGIN_ATTR = 'pyAccount'
    LDAP_BIND_DIRECT_GET_USER_INFO = True
    LDAP_USER_SEARCH_SCOPE = 'BASE'

    LDAP_ALWAYS_SEARCH_BIND = False
    LDAP_USER_RDN_ATTR = 'pyAccount'

    #USER AND ACCOUNT
    PASSWORD_MAX = 60
    PASSWORD_MIN = 6


class DefaultConfig(BaseConfig):

    DEBUG = True

    SENTRY_DSN = ""

    MAIL_HOST = ""
    FROM_ADDR = ""
    TO_ADDRS = [""]
    MAIL_USERNAME = ""
    MAIL_PASSWORD = ""

    # Flask-Sqlalchemy: http://packages.python.org/Flask-SQLAlchemy/config.html
    SQLALCHEMY_ECHO = False
    # QLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be
    # disabled by default in the future.
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # SQLITE for prototyping.
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + INSTANCE_FOLDER_PATH + '/db.sqlite'
    # MYSQL for production.
    #SQLALCHEMY_DATABASE_URI = 'mysql://username:password@server/db?charset=utf8'


class TestConfig(BaseConfig):
    TESTING = True
    CSRF_ENABLED = False
    WTF_CSRF_ENABLED = False

    SQLALCHEMY_ECHO = False
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
